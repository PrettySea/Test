#include <stdio.h>

#define MAX_NUM 10

void array_print(int array[], int len)
{
	int i = 0;
	for (i = 0; i<len; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

int insert_sort(int array[], int len) 
{     
    int i = 0;
    int j = 0;
    int temp =  0;
    for (i = 1; i < len; i++)				//默认第一个元素是有序，后面为无序
    {   
		if (array[i] < array[i-1])			// 若该元素比前一个元素大，说明前面元素都是有序的，不需要排序
		{
			for (j = i-1; j >= 0 && array[i] < array[j]; j--)	//结束条件：array[i] < array[j] 因为若是array[i]>array[j]就是有序的，不需要排序
			{             
				array[j+1] = array[j];	    //前一个元素值赋值给后一个元素
			}
			array[j+1] = array[i];			//内循环完后，需要存放array[i]，内循环结束后，j--，需j+1位置处存放array[i]
		}
        
		
		array_print(array, len);
    }
    return 0;
}

int main() 
{
    int i = 0;
	int array[MAX_NUM] ={0};
	for (i = 0; i < MAX_NUM; i++)
	{
		array[i] = rand()%MAX_NUM;
	}
	
    insert_sort(array, MAX_NUM);
	
    return 0; 
} 

